//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.xnet.oau;

import java.security.Key;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import sun.BASE64Decoder.Decoder.BASE64Decoder;
import sun.BASE64Decoder.Decoder.BASE64Encoder;

public class esdKeyGen {
    private static String algorithm = "AES";
    private static byte[] keyValue = new byte[]{49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 50, 51, 53, 53, 54, 55};

    public esdKeyGen() {
    }

    public static String encrypt(String plainText) {
        String encryptedValue = null;

        try {
            Key key = generateKey();
            Cipher e = Cipher.getInstance(algorithm);
            e.init(1, key);
            byte[] encVal = e.doFinal(plainText.getBytes());
            encryptedValue = (new BASE64Encoder()).encode(encVal);
        } catch (Exception var5) {
            var5.printStackTrace();
        }

        return encryptedValue;
    }

    public static String decrypt(String encryptedText) {
        String decryptedValue = null;

        try {
            Key key = generateKey();
            Cipher e = Cipher.getInstance(algorithm);
            e.init(2, key);
            byte[] decordedValue = (new BASE64Decoder()).decodeBuffer(encryptedText);
            byte[] decValue = e.doFinal(decordedValue);
            decryptedValue = new String(decValue);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

        return decryptedValue;
    }

    private static Key generateKey() throws Exception {
        SecretKeySpec key = new SecretKeySpec(keyValue, algorithm);
        return key;
    }

    public static void startTest() {
        String text = "SJ9T4AgrAlgCWFmXxow7iIOZsXp67tWYfQ4XOwDSEmcRr3FoMM546Mpc3qi5fW+5";
        String plainText = decrypt(text);
        System.out.println("File license Text : " + text);
        System.out.println("Encrypted Text : " + text);
        System.out.println("Decrypted Text : " + plainText);
        String new_license_date = "2018, 05, 18, ScaniaESD2017Update*#, main";
        String encryptedText = encrypt(new_license_date);
        System.out.println("Encrypted Text : " + encryptedText);
        String decryptedText = decrypt(encryptedText);
        System.out.println("Decrypted Text : " + decryptedText);
        String[] date_str_arr = decryptedText.split(",");
        int year = 2012;
        int month = 2;
        int day = 20;
        if (date_str_arr.length >= 3 && date_str_arr[3].trim().equals("ScaniaESD2017Update*#")) {
            year = Integer.parseInt(date_str_arr[0].trim());
            month = Integer.parseInt(date_str_arr[1].trim());
            day = Integer.parseInt(date_str_arr[2].trim());
        }

        Calendar expireDate = Calendar.getInstance();
        expireDate.set(year, month, day);
        LocalDate d101 = new LocalDate(Calendar.getInstance().getTimeInMillis());
        LocalDate d202 = new LocalDate(expireDate.getTimeInMillis());
        int days2 = Days.daysBetween(d101, d202).getDays();
        System.out.println("days2 : " + days2);
    }
}
