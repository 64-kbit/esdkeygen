//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.xnet.oau;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;

public class AppController {
    @FXML
    private Label label;
    @FXML
    private FlowPane mainAppPane;
    @FXML
    private Pane headerPane;
    @FXML
    private Pane controlPane;
    @FXML
    private Pane footerPane;
    @FXML
    private Button keyGenBtn;
    @FXML
    private DatePicker expireDateInputDate;
    @FXML
    private TextArea keyOutTextArea;
    @FXML
    private Label evenStatusLabel;
    @FXML
    private Label currentDateStatusLabel;

    public AppController() {
    }

    public void initialize() {
       // this.keyGenBtn.setOnAction(this::handleButtonAction);
        Date today = new Date();
        DateFormat df = DateFormat.getDateInstance(3, Locale.US);
        String strDate = df.format(today);
        this.currentDateStatusLabel.setText(strDate);
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        LocalDate date = (LocalDate) this.expireDateInputDate.getValue();
        if (date != null) {
            this.evenStatusLabel.setText("Expire: " + date);
            String new_license_date = ((LocalDate)this.expireDateInputDate.getValue()).getYear() + ", " + ((LocalDate)this.expireDateInputDate.getValue()).getMonthValue() + ", " + ((LocalDate)this.expireDateInputDate.getValue()).getDayOfMonth() + ", ScaniaESD2017Update*#, full";
            String encryptedText = esdKeyGen.encrypt(new_license_date);
            this.keyOutTextArea.setWrapText(true);
            this.keyOutTextArea.setText(encryptedText);
        } else {
            this.keyOutTextArea.setText("Empty Date");
            this.evenStatusLabel.setText("No Date");
        }

    }
}
